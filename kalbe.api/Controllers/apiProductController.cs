﻿using System.Security.Policy;
using System.Xml.Linq;
using kalbe.datamodels;
using kalbe.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace kalbe.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiProductController : ControllerBase
    {
        private readonly KALBEContext db;
        private VMResponse response = new VMResponse();

        public apiProductController(KALBEContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<Produk> GetAllData()
        {
            List<Produk> dataProduk = db.Produks.ToList();
            return dataProduk;
        }

        [HttpGet("GetDataProductById/{id}")]
        public Produk GetDataByIdProduct(int id)
        {
            Produk result = new Produk();
            result = db.Produks.Where(a => a.IntProductId == id).FirstOrDefault()!;
            return result;
        }

		[HttpGet("CheckProdukByName/{name}")]
		public bool CheckName(string name)
		{
			Produk data = db.Produks.Where(a => a.TxtProductName == name).FirstOrDefault()!;
			if (data != null)
			{
				return true;
			}
			return false;
		}

		[HttpPost("Save")]
        public VMResponse Save(Produk data)
        {
            data.DtInserted = DateTime.Now;
            data.TxtProductCode = GenerateCode();

            try
            {
                db.Add(data);
                db.SaveChanges();
                response.Message = "Data succes saved";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Failed saved" + ex.Message;
            }

            return response;
        }

        [HttpGet("GenerateCode")]
        public string GenerateCode()
        {
            string code = $"P";
            string digit = "";
            Produk data = db.Produks.OrderByDescending(a => a.TxtProductCode).FirstOrDefault()!;
            if (data != null)
            {
                string codeLast = data.TxtProductCode;
                string[] codeSplit = codeLast.Split("P");
                int intLast = int.Parse(codeSplit[1]) + 1;
                digit = intLast.ToString("0000");
            }
            else
                digit = "0001";

            return code + digit;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(Produk data)
        {
            Produk dt = db.Produks.Where(a => a.IntProductId == data.IntProductId).FirstOrDefault()!;

            if (dt != null)
            {
                dt.TxtProductName = data.TxtProductName;
                dt.IntQty = data.IntQty;
                dt.DecPrice = data.DecPrice;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    response.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = "Saved failed" + ex.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            Produk dt = db.Produks.Where(a => a.IntProductId == id).FirstOrDefault()!;
            if(dt != null)
            {
                try{
                    db.Remove(dt);
                    db.SaveChanges();
                }
                catch(Exception ex)
                {
                    response.Success = false;
                    response.Message = "Delete failed " + ex.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }

            return response;     
        }
    }
}
