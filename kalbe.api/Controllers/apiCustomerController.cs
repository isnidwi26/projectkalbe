﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using kalbe.datamodels;
using kalbe.viewmodels;

namespace kalbe.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerController : ControllerBase
    {
        private readonly KALBEContext db;
		private VMResponse response = new VMResponse();

		public apiCustomerController(KALBEContext _db)
		{
			this.db = _db;
		}

		[HttpGet("GetAllData")]
		public List<Customer> GetAllData()
		{
			List<Customer> dataCustomer = db.Customers.ToList();
			return dataCustomer;
		}

		[HttpGet("GetDataCustomerById/{id}")]
		public Customer GetDataByIdCustomer(int id)
		{
			Customer result = new Customer();
			result = db.Customers.Where(a => a.IntCustomerId == id).FirstOrDefault()!;
			return result;
		}

		[HttpGet("CheckCustomerByName/{name}")]
		public bool CheckName(string name)
		{
			Customer data = db.Customers.Where(a => a.TxtCustomerName == name).FirstOrDefault()!;
			if (data != null)
			{
				return true;
			}
			return false;
		}

		[HttpPost("Save")]
		public VMResponse Save(Customer data)
		{
			data.Inserted = DateTime.Now;
			try
			{
				db.Add(data);
				db.SaveChanges();
				response.Message = "Data succes saved";
			}
			catch (Exception ex)
			{
				response.Success = false;
				response.Message = "Failed saved" + ex.Message;
			}

			return response;
		}

		[HttpPut("Edit")]
		public VMResponse Edit(Customer data)
		{
			Customer dt = db.Customers.Where(a => a.IntCustomerId == data.IntCustomerId).FirstOrDefault()!;

			if (dt != null)
			{
				dt.TxtCustomerName = data.TxtCustomerName;
				dt.TxtCustomerAddress = data.TxtCustomerAddress;
				dt.DtmBirthDate = data.DtmBirthDate;
				dt.BitGender = data.BitGender;
				try
				{
					db.Update(dt);
					db.SaveChanges();
					response.Message = "Data success saved";
				}
				catch (Exception ex)
				{
					response.Success = false;
					response.Message = "Saved failed" + ex.Message;
				}
			}
			else
			{
				response.Success = false;
				response.Message = "Data not found";
			}
			return response;
		}

		[HttpDelete("Delete/{id}")]
		public VMResponse Delete(int id)
		{
			Customer dt = db.Customers.Where(a => a.IntCustomerId == id).FirstOrDefault()!;
			if (dt != null)
			{
				try
				{
					db.Remove(dt);
					db.SaveChanges();
				}
				catch (Exception ex)
				{
					response.Success = false;
					response.Message = "Delete failed " + ex.Message;
				}
			}
			else
			{
				response.Success = false;
				response.Message = "Data not found";
			}

			return response;
		}


	}
}
