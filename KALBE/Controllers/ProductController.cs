﻿using Microsoft.AspNetCore.Mvc;
using kalbe.datamodels;
using kalbe.viewmodels;
using kalbe.web.Services;

namespace kalbe.web.Controllers
{
    public class ProductController : Controller
    {
        private ProductService productService;

        public ProductController(ProductService _productService)
        {
            this.productService = _productService;
        }

        public async Task<IActionResult> Index(string sortOrder,
                                                string searchString,
                                                string currentFilter,
                                                int? pageNumber,
                                                int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            List<Produk> data = await productService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.TxtProductName.ToLower().Contains(searchString.ToLower())
                    ).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.TxtProductName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.TxtProductName).ToList();
                    break;
            }

            return View(PaginatedList<Produk>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));

        }

		public IActionResult Create()
		{
			Produk data = new Produk();
			return PartialView(data);
		}

		[HttpPost]
		public async Task<IActionResult> Create(Produk dataParam)
		{
			
			VMResponse respon = await productService.Create(dataParam);
			if (respon.Success)
			{
				return Json(new { dataRespon = respon });
			}
			return View(dataParam);
		}

		public async Task<JsonResult> CheckNameIsExist(string ProductName)
		{
			bool isExist = await productService.CheckProdukByName(ProductName);
			return Json(isExist);

		}

		public async Task<IActionResult> Detail(int id)
		{
			Produk data = await productService.GetDataById(id);
			return PartialView(data);
		}

		public async Task<IActionResult> Delete(int id)
		{
			Produk data = await productService.GetDataById(id);
			return PartialView(data);
		}

		public async Task<IActionResult> SureDelete(int id)
		{

			VMResponse respon = await productService.Delete(id);

			if (respon.Success)
			{
				return RedirectToAction("Index");
			}
			else
				return RedirectToAction("Index", id);
		}

        public async Task<IActionResult> Edit(int id)
        {
            Produk data = await productService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Produk dataParam)
        {
         
            VMResponse respon = await productService.Edit(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }
    }
}
