﻿using kalbe.datamodels;
using kalbe.viewmodels;
using kalbe.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace kalbe.web.Controllers
{
	public class CustomerController : Controller
	{
		private CustomerService customerService;

        public CustomerController(CustomerService _cutomerService)
        {
			this.customerService = _cutomerService;
        }
		public async Task<IActionResult> Index(string sortOrder,
												string searchString,
												string currentFilter,
												int? pageNumber,
												int? pageSize)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.CurrentPageSize = pageSize;
			ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
			if (searchString != null)
			{
				pageNumber = 1;
			}
			else
			{
				searchString = currentFilter;
			}
			ViewBag.CurrentFilter = searchString;
			List<Customer> data = await customerService.GetAllData();

			if (!string.IsNullOrEmpty(searchString))
			{
				data = data.Where(a => a.TxtCustomerName.ToLower().Contains(searchString.ToLower())
					).ToList();
			}

			switch (sortOrder)
			{
				case "name_desc":
					data = data.OrderByDescending(a => a.TxtCustomerName).ToList();
					break;
				default:
					data = data.OrderBy(a => a.TxtCustomerName).ToList();
					break;
			}

			return View(PaginatedList<Customer>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));

		}

		public IActionResult Create()
		{
			Customer data = new Customer();
			return PartialView(data);
		}

		[HttpPost]
		public async Task<IActionResult> Create(Customer dataParam)
		{

			VMResponse respon = await customerService.Create(dataParam);
			if (respon.Success)
			{
				return Json(new { dataRespon = respon });
			}
			return View(dataParam);
		}
		public async Task<JsonResult> CheckNameIsExist(string CustomerName)
		{
			bool isExist = await customerService.CheckCustomerName(CustomerName);
			return Json(isExist);

		}

		public async Task<IActionResult> Detail(int id)
		{
			Customer data = await customerService.GetDataById(id);
			return PartialView(data);
		}

		public async Task<IActionResult> Delete(int id)
		{
			Customer data = await customerService.GetDataById(id);
			return PartialView(data);
		}

		public async Task<IActionResult> SureDelete(int id)
		{

			VMResponse respon = await customerService.Delete(id);

			if (respon.Success)
			{
				return RedirectToAction("Index");
			}
			else
				return RedirectToAction("Index", id);
		}

		public async Task<IActionResult> Edit(int id)
		{
			Customer data = await customerService.GetDataById(id);
			return PartialView(data);
		}

		[HttpPost]
		public async Task<IActionResult> Edit(Customer dataParam)
		{

			VMResponse respon = await customerService.Edit(dataParam);
			if (respon.Success)
			{
				return Json(new { dataRespon = respon });
			}
			return View(dataParam);
		}

	}
}
