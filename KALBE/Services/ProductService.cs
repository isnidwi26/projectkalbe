﻿using System.Drawing;
using System.Text;
using kalbe.datamodels;
using kalbe.viewmodels;
using Newtonsoft.Json;

namespace kalbe.web.Services
{
    public class ProductService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();

        public ProductService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<Produk>> GetAllData()
        {
            List<Produk> data = new List<Produk>();
            string apirespon = await client.GetStringAsync(RouteAPI + "apiProduct/GetAllData");
            data = JsonConvert.DeserializeObject<List<Produk>>(apirespon)!;
            return data;
        }

		public async Task<VMResponse> Create(Produk dataParam)
		{
			//Proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);
			//Proses mengubah string menjadi json lalu dikirim sebagai Request Body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
			//Proses memanggil API dan mengirimkan Body
			var request = await client.PostAsync(RouteAPI + "apiProduct/Save", content);

			if (request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();
				//proses convert hasil respon dari API ke object
				response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				response.Success = false;
				response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
			}
			return response;
		}

		public async Task<bool> CheckProdukByName(string ProductName)
		{
			string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProduct/CheckProdukByName/{ProductName}");
			bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);

			return isExist;
		}

		public async Task<Produk> GetDataById(int id)
		{
			Produk data = new Produk();
			string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProduct/GetDataProductById/{id}");
			data = JsonConvert.DeserializeObject<Produk>(apiResponse)!;
			return data;
		}

		public async Task<VMResponse> Delete(int id)
		{
			var request = await client.DeleteAsync(RouteAPI + $"apiProduct/Delete/{id}");
			if (request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();
				//proses convert hasil respon dari API ke object
				response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				response.Success = false;
				response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
			}
			return response;
		}

        public async Task<VMResponse> Edit(Produk dataParam)
        {
            //Proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //Proses mengubah string menjadi json lalu dikirim sebagai Request Body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //Proses memanggil API dan mengirimkan Body
            var request = await client.PutAsync(RouteAPI + "apiProduct/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
            }
            return response;
        }

    }
}
