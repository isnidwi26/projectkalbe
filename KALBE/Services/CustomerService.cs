﻿using System.Text;
using kalbe.datamodels;
using kalbe.viewmodels;
using Newtonsoft.Json;

namespace kalbe.web.Services
{
    public class CustomerService
    {
		private static readonly HttpClient client = new HttpClient();
		private IConfiguration configuration;
		private string RouteAPI = "";
		private VMResponse response = new VMResponse();

		public CustomerService(IConfiguration _configuration)
		{
			this.configuration = _configuration;
			this.RouteAPI = this.configuration["RouteAPI"];
		}

		public async Task<List<Customer>> GetAllData()
		{
			List<Customer> data = new List<Customer>();
			string apirespon = await client.GetStringAsync(RouteAPI + "apiCustomer/GetAllData");
			data = JsonConvert.DeserializeObject<List<Customer>>(apirespon)!;
			return data;
		}

		public async Task<VMResponse> Create(Customer dataParam)
		{
			//Proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);
			//Proses mengubah string menjadi json lalu dikirim sebagai Request Body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
			//Proses memanggil API dan mengirimkan Body
			var request = await client.PostAsync(RouteAPI + "apiCustomer/Save", content);

			if (request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();		
				//proses convert hasil respon dari API ke object
				response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				response.Success = false;
				response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
			}
			return response;
		}

		public async Task<bool> CheckCustomerName(string CustomerName)
		{
			string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/CheckCustomerByName/{CustomerName}");
			bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);

			return isExist;
		}

		public async Task<Customer> GetDataById(int id)
		{
			Customer data = new Customer();
			string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/GetDataCustomerById/{id}");
			data = JsonConvert.DeserializeObject<Customer>(apiResponse)!;
			return data;
		}

		public async Task<VMResponse> Delete(int id)
		{
			var request = await client.DeleteAsync(RouteAPI + $"apiCustomer/Delete/{id}");
			if (request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();
				//proses convert hasil respon dari API ke object
				response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				response.Success = false;
				response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
			}
			return response;
		}

		public async Task<VMResponse> Edit(Customer dataParam)
		{
			//Proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);
			//Proses mengubah string menjadi json lalu dikirim sebagai Request Body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
			//Proses memanggil API dan mengirimkan Body
			var request = await client.PutAsync(RouteAPI + "apiCustomer/Edit", content);

			if (request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();
				//proses convert hasil respon dari API ke object
				response = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				response.Success = false;
				response.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
			}
			return response;
		}
	}
}
