﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kalbe.viewmodels
{
    public class VMProduk
    {
        public int IntProductId { get; set; }
        public string TxtProductCode { get; set; } = null!;
        public string TxtProductName { get; set; } = null!;
        public int IntQty { get; set; }
        public decimal DecPrice { get; set; }
        public DateTime? DtInserted { get; set; }
    }
}
