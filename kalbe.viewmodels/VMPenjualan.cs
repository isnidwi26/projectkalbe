﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kalbe.viewmodels
{
    public class VMPenjualan
    {
        public int IntSalesOrderId { get; set; }
        public int IntCustomerId { get; set; }
        public int IntProductId { get; set; }
        public DateTime DtSalesOrder { get; set; }
        public int IntQty { get; set; }
    }
}
