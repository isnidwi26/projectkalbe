﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kalbe.viewmodels
{
    public class VMCustomer
    {
        public int IntCustomerId { get; set; }
        public string TxtCustomerName { get; set; } = null!;
        public string TxtCustomerAddress { get; set; } = null!;
        public bool BitGender { get; set; }
        public DateTime DtmBirthDate { get; set; }
        public DateTime? Inserted { get; set; }
    }
}
