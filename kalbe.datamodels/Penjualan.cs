﻿using System;
using System.Collections.Generic;

namespace kalbe.datamodels
{
    public partial class Penjualan
    {
        public int IntSalesOrderId { get; set; }
        public int IntCustomerId { get; set; }
        public int IntProductId { get; set; }
        public DateTime DtSalesOrder { get; set; }
        public int IntQty { get; set; }
    }
}
