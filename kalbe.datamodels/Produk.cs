﻿using System;
using System.Collections.Generic;

namespace kalbe.datamodels
{
    public partial class Produk
    {
        public int IntProductId { get; set; }
        public string? TxtProductCode { get; set; }
        public string TxtProductName { get; set; } = null!;
        public int IntQty { get; set; }
        public decimal DecPrice { get; set; }
        public DateTime? DtInserted { get; set; }
    }
}
