﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace kalbe.datamodels
{
    public partial class KALBEContext : DbContext
    {
        public KALBEContext()
        {
        }

        public KALBEContext(DbContextOptions<KALBEContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Customer> Customers { get; set; } = null!;
        public virtual DbSet<Penjualan> Penjualans { get; set; } = null!;
        public virtual DbSet<Produk> Produks { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=localhost; Initial Catalog=KALBE;Trusted_Connection=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.IntCustomerId)
                    .HasName("PK__Customer__DD257E1A912A7F17");

                entity.ToTable("Customer");

                entity.Property(e => e.IntCustomerId).HasColumnName("intCustomerID");

                entity.Property(e => e.BitGender).HasColumnName("bitGender");

                entity.Property(e => e.DtmBirthDate)
                    .HasColumnType("datetime")
                    .HasColumnName("dtmBirthDate");

                entity.Property(e => e.Inserted)
                    .HasColumnType("datetime")
                    .HasColumnName("inserted");

                entity.Property(e => e.TxtCustomerAddress)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("txtCustomerAddress");

                entity.Property(e => e.TxtCustomerName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("txtCustomerName");
            });

            modelBuilder.Entity<Penjualan>(entity =>
            {
                entity.HasKey(e => e.IntSalesOrderId)
                    .HasName("PK__Penjuala__C8E6E865D9898383");

                entity.ToTable("Penjualan");

                entity.Property(e => e.IntSalesOrderId).HasColumnName("intSalesOrderID");

                entity.Property(e => e.DtSalesOrder)
                    .HasColumnType("datetime")
                    .HasColumnName("dtSalesOrder");

                entity.Property(e => e.IntCustomerId).HasColumnName("intCustomerID");

                entity.Property(e => e.IntProductId).HasColumnName("intProductID");

                entity.Property(e => e.IntQty).HasColumnName("intQty");
            });

            modelBuilder.Entity<Produk>(entity =>
            {
                entity.HasKey(e => e.IntProductId)
                    .HasName("PK__Produk__06E80B830E0EB880");

                entity.ToTable("Produk");

                entity.Property(e => e.IntProductId).HasColumnName("intProductID");

                entity.Property(e => e.DecPrice)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("decPrice");

                entity.Property(e => e.DtInserted)
                    .HasColumnType("datetime")
                    .HasColumnName("dtInserted");

                entity.Property(e => e.IntQty).HasColumnName("intQty");

                entity.Property(e => e.TxtProductCode)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("txtProductCode");

                entity.Property(e => e.TxtProductName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("txtProductName");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
