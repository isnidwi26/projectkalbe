﻿using System;
using System.Collections.Generic;

namespace kalbe.datamodels
{
    public partial class Customer
    {
        public int IntCustomerId { get; set; }
        public string TxtCustomerName { get; set; } = null!;
        public string TxtCustomerAddress { get; set; } = null!;
        public bool BitGender { get; set; }
        public DateTime DtmBirthDate { get; set; }
        public DateTime? Inserted { get; set; }
    }
}
